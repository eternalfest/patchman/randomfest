# @patchman/randomfest

Haxe reimplementation of Randomfest.
Contains the bottom bar mod as it is not a package yet.
However it is currently missing a fix for evaluating a e_portal when dimensions are activated which cannot be re-evaluated later.
