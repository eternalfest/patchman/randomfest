package randomfest;

import etwin.ds.Set;
import etwin.ds.Nil;
import etwin.flash.Key;
import etwin.flash.MovieClip;
import etwin.Obfu;
import hf.GameManager;
import hf.Entity;
import hf.SpecialManager;
import hf.entity.Bomb;
import hf.entity.Player;
import hf.entity.PlayerController;
import hf.mode.Adventure;
import hf.mode.GameMode;
import hf.mode.MultiCoop;
import hf.Hf;
import hf.Std;
import patchman.HostModuleLoader;
import patchman.IPatch;
import patchman.Ref;
import patchman.VoidPatch;
import bottom_bar.modules.BottomBarModuleFactory;
import color_matrix.ColorMatrix;
import sky_ui.SkyUI;
import keyboard.KeyCode;
import randomfest.RandomfestConfig;
import randomfest.RandomfestLog;
import randomfest.RandomfestBottomBar;

private enum LeftRightChoice {
    NO_LEFT_RIGHT_CHOICE;
    LEFT_CHOICE;
    RIGHT_CHOICE;
}

@:build(patchman.Build.di())
class Randomfest {
    private static var optionName(default, never): String = Obfu.raw("randomfest");

    // TODO: Also in shadow clones. To remove when refactored somewhere.
    private static function isOptionEnabled(loader: HostModuleLoader): Bool {
        var run = loader.require(Obfu.raw("run")).getRun();
        var options: Array<Dynamic> = run.gameOptions;
        for (i in 0...(options.length)) {
            if (options[i] == optionName)
                return true;
        }
        return false;
    }

    private var loader: HostModuleLoader;
    public function isEnabled(): Bool {
        return isOptionEnabled(loader);
    }

    /* Currently configurable. */
    private var nbInitialRemainingControl: Int;
    private var areDimensionsAlwaysActivated: Bool;
    private var nbDeathForControlGain: Int;
    private var nbAdditionalPlayers: Int;
    private var allowSpawningRandoms: Bool;

    // TODO: Also randomfest for ololzd2 will need to allow saving the objects (so it should be configurable when coded).

    private static var mainEternalNames(default, never): Array<String> = [Obfu.raw("Eternalfest"), Obfu.raw("EternalPGM"), Obfu.raw("EternalSky"), Obfu.raw("EternalGorithm"), Obfu.raw("EternalSadist"), Obfu.raw("EternalCool"), Obfu.raw("EternalBot"), Obfu.raw("EternalArming"), Obfu.raw("EternalLiteration"), Obfu.raw("EternalLegro"), Obfu.raw("EternalGebra"), Obfu.raw("EternalEd"), Obfu.raw("GardienTO")];
    private static var oldEternalNames(default, never): Array<String> = [Obfu.raw("EternalCruel"), Obfu.raw("EternalAbourre"), Obfu.raw("EternalLele"), Obfu.raw("EternalChemist"), Obfu.raw("EternalKali"), Obfu.raw("EternalSnake")];
    private static var adminNames(default, never): Array<String> = [Obfu.raw("deepnight"), Obfu.raw("ayame"), Obfu.raw("hiko")];
    private static var playerNames(default, never): Array<String> = [Obfu.raw("2skik7"), Obfu.raw("6avalanche"), Obfu.raw("Aerynsun"), Obfu.raw("Aksamyt"), Obfu.raw("alexouief"), Obfu.raw("arounet"), Obfu.raw("assiette"), Obfu.raw("babast88"), Obfu.raw("Badboy67"), Obfu.raw("BanwasBEST"), Obfu.raw("Benlapin"), Obfu.raw("betti291"), Obfu.raw("bc"), Obfu.raw("BlessedRacc"), Obfu.raw("byakuya95"), Obfu.raw("carmonaaaaaa"), Obfu.raw("Cirerd2"), Obfu.raw("codsekret"), Obfu.raw("continuum"), Obfu.raw("Crapaud21"), Obfu.raw("darkkie"), Obfu.raw("destructeur"), Obfu.raw("diccy"), Obfu.raw("drimnas"), Obfu.raw("eipi10"), Obfu.raw("Elagea"), Obfu.raw("Elseabora"), Obfu.raw("Erneraude"), Obfu.raw("fabianh"), Obfu.raw("fardur23"), Obfu.raw("fatal2412"), Obfu.raw("Flavinus1234"), Obfu.raw("Galenus"), Obfu.raw("gallbatorix"), Obfu.raw("GlobZOsiris"), Obfu.raw("gigusu"), Obfu.raw("globox62"), Obfu.raw("gmnemo"), Obfu.raw("gollumsk8"), Obfu.raw("guigui1001"), Obfu.raw("ironmanfour"), Obfu.raw("ivabra17"), Obfu.raw("Jean-Michel Tractoprank"), Obfu.raw("jjdu51"), Obfu.raw("jules93"), Obfu.raw("Kaeldori"), Obfu.raw("kahakatashi"), Obfu.raw("krokodebil"), Obfu.raw("Kans"), Obfu.raw("KURO18"), Obfu.raw("lekapi"), Obfu.raw("lokiwider"), Obfu.raw("lougarelou11"), Obfu.raw("lyokobis5"), Obfu.raw("Mandine"), Obfu.raw("maniaclan"), Obfu.raw("max8312083"), Obfu.raw("Maxadona"), Obfu.raw("maxdefolsch"), Obfu.raw("Maxxfit"), Obfu.raw("milaanarz"), Obfu.raw("Momo476"), Obfu.raw("midinou"), Obfu.raw("m00ns0rr0w"), Obfu.raw("moejul"), Obfu.raw("moulins"), Obfu.raw("mrlaventure"), Obfu.raw("mrloxa"), Obfu.raw("Muttter"), Obfu.raw("Mva54"), Obfu.raw("Nammerfest"), Obfu.raw("Nassimou"), Obfu.raw("nonoleboss"), Obfu.raw("Passage04"), Obfu.raw("poiromi"), Obfu.raw("psg9535"), Obfu.raw("Pytytange"), Obfu.raw("rollerfou"), Obfu.raw("sajd"), Obfu.raw("Sewerman"), Obfu.raw("Simon"), Obfu.raw("stef87"), Obfu.raw("steph0808"), Obfu.raw("stratus"), Obfu.raw("Syl57"), Obfu.raw("Tactize"), Obfu.raw("Tchoobaka"), Obfu.raw("Teraka91"), Obfu.raw("Thepoule"), Obfu.raw("Toligwen"), Obfu.raw("Tolkiendil"), Obfu.raw("vacerne"), Obfu.raw("Vaxos"), Obfu.raw("Xirty"), Obfu.raw("yamifred"), Obfu.raw("yuuty"), Obfu.raw("Zryxxx")];
    private static var probabilityMainEternalBeingReplaced(default, never): Float = 0.3;
    private static var probabilityReplacementIsAdmin(default, never): Float = 0.03;
    private static var probabilitiesPerFrame(default, never): Dynamic = {
        attack: 0.05,
        liftBomb: 0.05,
        jump: 0.0225,
        changeHorizontalChoice: 0.04,

        attackVariance: 0.02,
        liftBombVariance: 0.02,
        jumpVariance: 0.01,
        changeHorizontalChoiceVariance: 0.01
    };

    private var remainingMainNames: Array<String>;
    private var remainingAdminNames: Array<String>;
    private var remainingOtherNames: Array<String>;

    private var nbDeathTotal: Int = 0;
    private var nbDeathCurrentLevel: Int = 0;
    private var currentLevelId: {did: Int, lid: Int};
    private var log: RandomfestLog;
    private var totalActiveFrames: Int = 0;
    private var inControl: Bool = false;
    private var dimensionsActivated: Bool = false;
    private var previousPlayerPosition: Map<Player, {x: Null<Float>, y: Null<Float>}>;
    private var previousLeftRightChoice: Map<Player, LeftRightChoice>;
    private var leftRightChoice: Map<Player, LeftRightChoice>;
    private var attackChoice: Map<Player, Bool>;
    private var jumpChoice: Map<Player, Bool>;
    private var downChoice: Map<Player, Bool>;
    private var playerProbabilities: Map<Player, {attack: Float, liftBomb: Float, jump: Float, changeHorizontalChoice: Float}>;
    private var playerNameSprite: Map<Player, MovieClip>;
    private var sparklingPlayers: Set<Player>;
    private var nbRemainingControl: Int = 3;

    /* Those public getters are for the custom bar. */
    public function getNbDeathTotal(): Int {
        return nbDeathTotal;
    }
    public function getNbRemainingControl(): Int {
        return nbRemainingControl;
    }
    public function getNbDeathCurrentLevel(): Int {
        return nbDeathCurrentLevel;
    }

    private static function selectProbaAround(normalProbability, variance) {
        var difference = (Math.random() * 2 - 1) * variance;
        return Math.min(Math.max(0, normalProbability + difference), 1);
    }

    public function isRandomfestPlayer(player: Player): Bool {
        /* If it's a randomfest player, then it should have the random data, so check any. */
        return leftRightChoice.exists(player);
    }

    private function initRandomChoices(player: Player): Void {
        previousPlayerPosition.set(player, {x: null, y: null});
        previousLeftRightChoice.set(player, NO_LEFT_RIGHT_CHOICE);
        leftRightChoice.set(player, NO_LEFT_RIGHT_CHOICE);
        attackChoice.set(player, false);
        jumpChoice.set(player, false);
        downChoice.set(player, false);
        playerProbabilities.set(player, {
            attack: selectProbaAround(probabilitiesPerFrame.attack, probabilitiesPerFrame.attackVariance),
            liftBomb: selectProbaAround(probabilitiesPerFrame.liftBomb, probabilitiesPerFrame.liftBombVariance),
            jump: selectProbaAround(probabilitiesPerFrame.jump, probabilitiesPerFrame.jumpVariance),
            changeHorizontalChoice: selectProbaAround(probabilitiesPerFrame.changeHorizontalChoice, probabilitiesPerFrame.changeHorizontalChoiceVariance)
        });
    }

    private static function sampleAndExtract(hf: Hf, array: Array<String>) {
        var randomIndex = hf.Std.random(array.length);
        var randomValue = array[randomIndex];
        array.splice(randomIndex, 1);
        return randomValue;
    };

    private function reinitializePossibleNames(): Void {
        remainingMainNames = mainEternalNames.copy();
        remainingAdminNames = adminNames.copy();
        remainingOtherNames = oldEternalNames.copy().concat(playerNames.copy());
    }

    private function getNewRandomIdentity(hf: Hf): {name: String, isAdmin: Bool} {
        var hasRemainingMain: Bool = remainingMainNames.length > 0;
        var hasRemainingAdmin: Bool = remainingAdminNames.length > 0;
        var hasRemainingOther: Bool = remainingOtherNames.length > 0;

        /* If too much are ever spawned (however unlikely that is...), restart a new cycle of possible names. */
        if (!hasRemainingMain && !hasRemainingAdmin && !hasRemainingOther) {
            reinitializePossibleNames();
            return getNewRandomIdentity(hf);
        }

        var replaceName: Bool = !hasRemainingMain || ((hasRemainingAdmin || hasRemainingOther) && Math.random() < probabilityMainEternalBeingReplaced);
        if (replaceName) {
            var useAdmin: Bool = !hasRemainingOther || (hasRemainingAdmin && Math.random() < probabilityReplacementIsAdmin);
            if (useAdmin)
                return {name: sampleAndExtract(hf, remainingAdminNames), isAdmin: true};
            else
                return {name: sampleAndExtract(hf, remainingOtherNames), isAdmin: false};
        }
        else {
            return {name: sampleAndExtract(hf, remainingMainNames), isAdmin: false};
        }
    }

    private function setPlayerData(hf: Hf, game: GameMode, player: Player): Void {
        playerNameSprite.set(player, SkyUI.CreateNormalTextSprite(hf, game, player.name, player.x, player.y - 65, 100, hf.Data.DP_PLAYER, TextAlignmentCenter));
        initRandomChoices(player);
    }

    private function canSpawnRandomPlayers(): Bool {
        return allowSpawningRandoms || isEnabled();
    }

    public function spawnRandomPlayer(hf: Hf, game: GameMode, x: Float, y: Float): Null<Player> {
        if (!canSpawnRandomPlayers())
            return null;

        var identity: {name: String, isAdmin: Bool} = getNewRandomIdentity(hf);
        var playerName = identity.name;
        var player = game.insertPlayer(x, y);
        player.ctrl.setKeys(-1, -1, -1, -1, -1);
        player.name = playerName;
        player.skin = hf.Std.random(4) + 1;
        player.head = hf.Std.random(12) + 1;
        player.fl_carot = hf.Std.random(2) == 0;
        if (identity.isAdmin)
            sparklingPlayers.add(player);
        player.replayAnim();
        var transformValue = playerName == Obfu.raw("EternalArming") ? -1 : 1;
        player.filters = [ColorMatrix.fromHsv(hf.Std.random(360), 1, transformValue).toFilter()];
        /* insertPlayer calls .hide() on it at the end, we want it directly visible. */
        player.show();
        setPlayerData(hf, game, player);
        return player;
    }

    private function spawnRandomPlayers(hf: Hf, game: GameMode): Void {
        for (i in 0...nbAdditionalPlayers)
            spawnRandomPlayer(hf, game, hf.Std.random(24), -2);
    }

    private function updateRandomChoices(hf: Hf, player: Player): Void {
        var playerProba = playerProbabilities.get(player);
        previousLeftRightChoice.set(player, leftRightChoice.get(player));

        attackChoice.set(player, Math.random() < playerProba.attack);
        downChoice.set(player, Math.random() < playerProba.liftBomb);
        jumpChoice.set(player, Math.random() < playerProba.jump);

        if (Math.random() < playerProba.changeHorizontalChoice) {
            var possibleLeftRightChoices = [NO_LEFT_RIGHT_CHOICE, LEFT_CHOICE, RIGHT_CHOICE];
            leftRightChoice.set(player, possibleLeftRightChoices[hf.Std.random(possibleLeftRightChoices.length)]);
        }
        else {
            leftRightChoice.set(player, previousLeftRightChoice.get(player));
        }

        /* Against a wall, trying to move into it. */
        if (leftRightChoice.get(player) == previousLeftRightChoice.get(player) && leftRightChoice.get(player) != NO_LEFT_RIGHT_CHOICE
            && previousPlayerPosition.get(player).x == player.x && previousPlayerPosition.get(player).y == player.y) {
            var randomChoice = Math.random();
            if (randomChoice < 0.20) {
                leftRightChoice.set(player, NO_LEFT_RIGHT_CHOICE);
            }
            else if (randomChoice < 0.60) {
                if (leftRightChoice.get(player) == LEFT_CHOICE)
                    leftRightChoice.set(player, RIGHT_CHOICE);
                else
                    leftRightChoice.set(player, LEFT_CHOICE);
            }
            else {
                jumpChoice.set(player, true);
            }
        }

        previousPlayerPosition.set(player, {x: player.x, y: player.y});
    }

    private function makePlayerNamesInvisible(players: Array<Player>): Void {
        for (player in players)
            if (isRandomfestPlayer(player))
                playerNameSprite.get(player)._visible = false;
    }

    private function makePlayerNamesVisible(players: Array<Player>): Void {
        for (player in players)
            if (isRandomfestPlayer(player))
                playerNameSprite.get(player)._visible = true;
    }

    /* See FxManager.attachAlert. */
    // TODO: Move to SkyUI with configurable scale?
    private static function createCenterTextSprite(hf: Hf, game: GameMode, text: String) {
        var textSprite: MovieClip = cast game.depthMan.attach("hurryUp", hf.Data.DP_INTERF);
        textSprite._x = hf.Data.GAME_WIDTH * 0.5;
        textSprite._y = hf.Data.GAME_HEIGHT * 0.5;
        (cast textSprite).label = text;
        textSprite._xscale = 70;
        textSprite._yscale = 70;
        game.fxMan.mcList.push(textSprite);
        game.fxMan.lastAlert = textSprite;
        return textSprite;
    }

    public function randomfestTimeToString(): String {
        /* If there wasn't any lag, there would be 40 frames per seconds. So:
        *  - 1 frame is 25 thousandth of a second.
        *  - 1 second is 40 frames.
        *  - 1 minute is 2400 frames.
        *  - 1 hour is 144000 frames.
        *  - 1 day is 3456000 frames. */
        var days: Int = Math.floor(totalActiveFrames / 3456000);
        var hoursInDay: Int = Math.floor(totalActiveFrames / 144000) % 24;
        var minutesInHour: Int = Math.floor(totalActiveFrames / 2400) % 60;
        var secondsInMinutes: Int = Math.floor(totalActiveFrames / 40) % 60;
        var remainingFrames: Int = totalActiveFrames % 40;
        var thousandthSecondInSecond: Int = remainingFrames * 25;
        /* Only display the days if there's any, as it doesn't happen much. */
        var daysString: String = days > 0 ? days + Obfu.raw("j") : "";
        return daysString + hoursInDay + Obfu.raw("h") + minutesInHour + Obfu.raw("m") + secondsInMinutes + Obfu.raw("s") + thousandthSecondInSecond;
    }

    @:diExport
    public var spawnRandomPlayersInSolo(default, null): IPatch;
    @:diExport
    public var spawnRandomPlayersInMulti(default, null): IPatch;
    @:diExport
    public var cleanPlayerStateOnPlayerDestructor(default, null): IPatch;
    @:diExport
    public var initPlayerRandomfestData(default, null): IPatch;
    @:diExport
    public var updatePlayerNameAndSparkles(default, null): IPatch;
    @:diExport
    public var manageRandomfestControls(default, null): IPatch;
    @:diExport
    public var replacePlayerControlsWithRandomfest(default, null): IPatch;
    @:diExport
    public var forbidDimensionsIfDisabled(default, null): IPatch;
    @:diExport
    public var hidePlayerNamesOnLevelEndAndStopControl(default, null): IPatch;
    @:diExport
    public var showPlayerNamesOnLevelBegin(default, null): IPatch;
    @:diExport
    public var infiniteLivesAndLogAndChechNewControl(default, null): IPatch;
    @:diExport
    public var removeHfLogs(default, null): IPatch;
    @:diExport
    public var removeMapIcons(default, null): IPatch;
    @:diExport
    public var logCristal(default, null): IPatch;
    @:diExport
    public var logSpecialItem(default, null): IPatch;
    @:diExport
    public var forceAbilityToSetDarknessFromBegin(default, null): IPatch;
    @:diExport
    public var replacePauseWithPlayerLog(default, null): IPatch;
    @:diExport
    public var replaceMapWithRandomfestLog(default, null): IPatch;
    @:diExport
    public var removeCustomLogOnResume(default, null): IPatch;

    public function new(loader: HostModuleLoader, randomfestConfig: RandomfestConfig): Void {
        this.loader = loader;
        this.allowSpawningRandoms = randomfestConfig.allowSpawningRandoms;

        if (canSpawnRandomPlayers()) {
            reinitializePossibleNames();
            log = new RandomfestLog();
            previousPlayerPosition = new Map<Player, {x: Null<Float>, y: Null<Float>}>();
            previousLeftRightChoice = new Map<Player, LeftRightChoice>();
            leftRightChoice = new Map<Player, LeftRightChoice>();
            attackChoice = new Map<Player, Bool>();
            jumpChoice = new Map<Player, Bool>();
            downChoice = new Map<Player, Bool>();
            playerProbabilities = new Map<Player, {attack: Float, liftBomb: Float, jump: Float, changeHorizontalChoice: Float}>();
            playerNameSprite = new Map<Player, MovieClip>();
            sparklingPlayers = new Set<Player>();
            this.nbInitialRemainingControl = randomfestConfig.nbInitialRemainingControl;
            this.areDimensionsAlwaysActivated = randomfestConfig.areDimensionsAlwaysActivated;
            this.nbDeathForControlGain = randomfestConfig.nbDeathForControlGain;
            this.nbAdditionalPlayers = randomfestConfig.nbAdditionalPlayers;
            nbRemainingControl = nbInitialRemainingControl;

            cleanPlayerStateOnPlayerDestructor = Ref.auto(Player.destroy).before(function(hf: Hf, self: Player): Void {
                if (!isRandomfestPlayer(self))
                    return;

                previousPlayerPosition.remove(self);
                previousLeftRightChoice.remove(self);
                leftRightChoice.remove(self);
                attackChoice.remove(self);
                jumpChoice.remove(self);
                downChoice.remove(self);
                playerProbabilities.remove(self);
                playerNameSprite.get(self).removeMovieClip();
                playerNameSprite.remove(self);
                if (sparklingPlayers.exists(self))
                    sparklingPlayers.remove(self);
            });

            updatePlayerNameAndSparkles = Ref.auto(Player.update).after(function(hf: Hf, self: Player): Void {
                if (!isRandomfestPlayer(self))
                    return;

                var textName = cast playerNameSprite.get(self);
                /* We need to update the name because they are set at awkward points (especially if we want to take party mode into account). */
                textName.field.text = self.name;
                textName.field._x = self.x - textName.field._width / 2;
                textName.field._y = self.y - 65;
                if (sparklingPlayers.exists(self)) {
                    if (Math.random() < 0.85) {
                        var sparkle = self.game.fxMan.attachFx(self.x + hf.Std.random(20) - 10, self.y + hf.Std.random(20) - 20, Obfu.raw("$ting"));
                        sparkle.fl_loopDone = true;
                        sparkle.lifeTimer = 40;
                    }
                }
            });

            /* Mostly a copy-paste or the original function with some changes where key inputs are checked... */
            // TODO: If someone has a better way to do this, I'm interested...
            replacePlayerControlsWithRandomfest = Ref.auto(PlayerController.getControls).prefix(function(hf: Hf, self: PlayerController): Nil<Void> {
                var player: Player = self.player;
                if (!isRandomfestPlayer(player))
                    return Nil.none();

                var leftKeyDown: Bool;
                var rightKeyDown: Bool;
                var jumpKeyDown: Bool;
                var attackKeyDown: Bool;
                var downKeyDown: Bool;
                if (inControl) {
                    leftKeyDown = Key.isDown(self.left);
                    rightKeyDown = Key.isDown(self.right);
                    jumpKeyDown = Key.isDown(self.jump);
                    attackKeyDown = self.keyIsDown(self.attack);
                    downKeyDown = self.keyIsDown(self.down);
                }
                else {
                    updateRandomChoices(hf, player);
                    leftKeyDown = leftRightChoice.get(player) == LEFT_CHOICE;
                    rightKeyDown = leftRightChoice.get(player) == RIGHT_CHOICE;
                    jumpKeyDown = jumpChoice.get(player);
                    attackKeyDown = attackChoice.get(player);
                    downKeyDown = downChoice.get(player);
                }

                var v4;
                var v3;
                var v2 = 0;
                while (v2 < self.lastKeys.length) {
                    if (!Key.isDown(self.lastKeys[v2])) {
                        self.keyLocks[self.lastKeys[v2]] = false;
                        self.lastKeys.splice(v2, 1);
                        --v2;
                    }
                    ++v2;
                }
                if (self.player.fl_stable) {
                    self.waterJump = 3;
                }
                if (leftKeyDown) {
                    if (self.game.fl_ice || self.game.fl_aqua) {
                        if (!self.player.fl_stable && self.game.fl_ice) {
                            v3 = 0.35;
                        } else {
                            v3 = 0.1;
                        }
                        self.player.dx -= v3 * hf.Data.PLAYER_SPEED * self.player.speedFactor;
                        self.player.dx = Math.max(self.player.dx, -hf.Data.PLAYER_SPEED * self.player.speedFactor);
                    } else {
                        self.player.dx = -hf.Data.PLAYER_SPEED * self.player.speedFactor;
                    }
                    self.player.dir = -1;
                    if (self.player.fl_stable) {
                        self.player.playAnim(self.player.baseWalkAnim);
                    }
                }
                if (rightKeyDown) {
                    if (self.game.fl_ice || self.game.fl_aqua) {
                        if (!self.player.fl_stable && self.game.fl_ice) {
                            v4 = 0.35;
                        } else {
                            v4 = 0.1;
                        }
                        self.player.dx += v4 * hf.Data.PLAYER_SPEED * self.player.speedFactor;
                        self.player.dx = Math.min(self.player.dx, hf.Data.PLAYER_SPEED * self.player.speedFactor);
                    } else {
                        self.player.dx = hf.Data.PLAYER_SPEED * self.player.speedFactor;
                    }
                    self.player.dir = 1;
                    if (self.player.fl_stable) {
                        self.player.playAnim(self.player.baseWalkAnim);
                    }
                }
                if (self.player.specialMan.actives[73]) {
                    if (self.player.fl_stable && self.player.dx != 0) {
                        self.walkTimer -= hf.Timer.tmod;
                        if (self.walkTimer <= 0) {
                            self.walkTimer = hf.Data.SECOND;
                            self.player.getScore(self.player, 10);
                        }
                    }
                }
                if (!leftKeyDown && !rightKeyDown) {
                    if (!self.game.fl_ice) {
                        self.player.dx *= self.game.gFriction * 0.8;
                    }
                    if (self.player.animId == self.player.baseWalkAnim.id || self.player.animId == hf.Data.ANIM_PLAYER_RUN.id) {
                        self.player.playAnim(self.player.baseStopAnim);
                    }
                }
                if (self.game.fl_aqua && self.waterJump > 0) {
                    if (!self.player.fl_stable && jumpKeyDown) {
                        self.player.airJump();
                        --self.waterJump;
                    }
                }
                if (self.player.fl_stable && jumpKeyDown) {
                    if (self.player.specialMan.actives[88]) {
                        self.player.dy = -hf.Data.PLAYER_JUMP * 0.5;
                    } else {
                        self.player.dy = -hf.Data.PLAYER_JUMP;
                    }
                    self.game.soundMan.playSound("sound_jump", hf.Data.CHAN_PLAYER);
                    self.player.playAnim(hf.Data.ANIM_PLAYER_JUMP_UP);
                    var v5 = self.game.fxMan.attachFx(self.player.x, self.player.y, "hammer_fx_jump");
                    v5.mc._alpha = 50;
                    if (self.player.specialMan.actives[66]) {
                        self.player.getScore(self.player, 10);
                    }
                    self.game.statsMan.inc(hf.Data.STAT_JUMP, 1);
                    self.lockKey(self.jump);
                }
                if (attackKeyDown && self.player.coolDown == 0) {
                    var v6 = hf.Data.KICK_DISTANCE;
                    if (!self.player.fl_stable) {
                        v6 = hf.Data.AIR_KICK_DISTANCE;
                    }
                    if (self.player.specialMan.actives[115]) {
                        v6 *= 1.2;
                    }
                    var v7: Array<Bomb> = cast self.game.getClose(hf.Data.BOMB, self.player.x, self.player.y, v6, false);
                    if (v7.length == 0) {
                        if (self.player.currentWeapon > 0 && self.player.countBombs() < self.player.maxBombs) {
                            var v8 = self.player.attack();
                            if (self.game.fl_bombControl && v8.isType(hf.Data.BOMB)) {
                                var v9: hf.entity.bomb.PlayerBomb = cast v8;
                                var v10 = hf.entity.WalkingBomb.attach(self.game, v9);
                            }
                            if (v8 != null) {
                                v8.setParent(self.player);
                            }
                            if (self.player.fl_stable) {
                                self.player.playAnim(hf.Data.ANIM_PLAYER_ATTACK);
                            }
                        }
                    } else {
                        if (self.fl_powerControl && self.player.fl_stable) {
                            var v11 = Math.min(1.2, 0.5 + Math.abs(self.player.dx) / 4);
                            self.player.kickBomb(v7, v11);
                        } else {
                            self.player.kickBomb(v7, 1.0);
                        }
                    }
                } else {
                    if (downKeyDown && self.fl_upKick) {
                        var v12 = hf.Data.KICK_DISTANCE;
                        if (!self.player.fl_stable) {
                            v12 = hf.Data.AIR_KICK_DISTANCE;
                        }
                        if (self.player.specialMan.actives[115]) {
                            v12 *= 1.2;
                        }
                        var v13: Array<Bomb> = cast self.game.getClose(hf.Data.BOMB, self.player.x, self.player.y, v12, false);
                        if (v13.length > 0) {
                            self.player.upKickBomb(v13);
                        }
                    }
                }

                return Nil.some(null);
            });

            hidePlayerNamesOnLevelEndAndStopControl = Ref.auto(GameMode.clearLevel).before(function(hf: Hf, self: GameMode): Void {
                makePlayerNamesInvisible(self.getPlayerList());

                log.logLevelSummary(currentLevelId, nbDeathCurrentLevel, randomfestTimeToString());
                inControl = false;
                dimensionsActivated = false;
                nbDeathCurrentLevel = 0;
            });

            showPlayerNamesOnLevelBegin = Ref.auto(Adventure.startLevel).after(function(hf: Hf, self: Adventure): Void {
                makePlayerNamesVisible(self.getPlayerList());

                currentLevelId = {did: self.currentDim, lid: self.world.currentId};
            });

            infiniteLivesAndLogAndChechNewControl = Ref.auto(Player.killPlayer).before(function(hf: Hf, self: Player): Void {
                if (!isRandomfestPlayer(self))
                    return;

                if (self.lives <= 0)
                    self.lives = 1;

                ++nbDeathTotal;
                if ((nbDeathTotal / nbDeathForControlGain - Math.floor(nbDeathTotal / nbDeathForControlGain)) == 0){
                    ++nbRemainingControl;
                    log.logControlGain(currentLevelId);
                    createCenterTextSprite(hf, self.game, "Gain d'une reprise de contrôle !");
                }
                ++nbDeathCurrentLevel;
            });
        }
        else {
            cleanPlayerStateOnPlayerDestructor = new VoidPatch();
            updatePlayerNameAndSparkles = new VoidPatch();
            replacePlayerControlsWithRandomfest = new VoidPatch();
            hidePlayerNamesOnLevelEndAndStopControl = new VoidPatch();
            showPlayerNamesOnLevelBegin = new VoidPatch();
            infiniteLivesAndLogAndChechNewControl = new VoidPatch();
        }

        if (!isEnabled()) {
            spawnRandomPlayersInSolo = new VoidPatch();
            spawnRandomPlayersInMulti = new VoidPatch();
            initPlayerRandomfestData = new VoidPatch();
            manageRandomfestControls = new VoidPatch();
            forbidDimensionsIfDisabled = new VoidPatch();
            removeHfLogs = new VoidPatch();
            removeMapIcons = new VoidPatch();
            logCristal = new VoidPatch();
            logSpecialItem = new VoidPatch();
            forceAbilityToSetDarknessFromBegin = new VoidPatch();
            replacePauseWithPlayerLog = new VoidPatch();
            replaceMapWithRandomfestLog = new VoidPatch();
            removeCustomLogOnResume = new VoidPatch();
            return;
        }

        BottomBarModuleFactory.get().addModule(Obfu.raw("Randomfest"), function(data: Dynamic) {return new RandomfestBottomBar(this);});

        // TODO: Potentially disable save of objects (and score?).

        spawnRandomPlayersInSolo = Ref.auto(Adventure.initGame).after(function(hf: Hf, self: Adventure): Void {
            spawnRandomPlayers(hf, self);
        });

        /* We cannot just do it in GameMode or in Adventure, because Multi starts by destroying all players... */
        spawnRandomPlayersInMulti = Ref.auto(MultiCoop.initGame).after(function(hf: Hf, self: MultiCoop): Void {
            spawnRandomPlayers(hf, self);
        });

        initPlayerRandomfestData = Ref.auto(Player.initPlayer).after(function(hf: Hf, self: Player, game: GameMode, x: Float, y: Float): Void {
            if (!isRandomfestPlayer(self))
                setPlayerData(hf, game, self);
        });

        manageRandomfestControls = Ref.auto(GameMode.getControls).before(function(hf: Hf, self: GameMode): Void {
            // TODO: Use KeyCode everywhere? Import as rename and use it here while keeping ints in controls remap?
            if (!self.fl_lock) {
                if (Key.isDown(KeyCode.R.toInt()) && !inControl && nbRemainingControl > 0) {
                    inControl = true;
                    --nbRemainingControl;
                    createCenterTextSprite(hf, self, "Reprise de contrôle !");
                    log.logControlUse(currentLevelId, randomfestTimeToString());
                }
                if (!areDimensionsAlwaysActivated && Key.isDown(KeyCode.E.toInt()) && !dimensionsActivated) {
                    dimensionsActivated = true;
                    createCenterTextSprite(hf, self, "Activation des dimensions !");
                }
                ++totalActiveFrames;
            }
            else {
                if (Key.isDown(KeyCode.UP.toInt()))
                    log.ScrollLog(hf, self, true);
                if (Key.isDown(KeyCode.DOWN.toInt()))
                    log.ScrollLog(hf, self, false);
            }
        });

        forbidDimensionsIfDisabled = Ref.auto(GameMode.usePortal).wrap(function(hf: Hf, self: GameMode, pid: Int, e: Entity, old): Bool {
            if (!areDimensionsAlwaysActivated && !dimensionsActivated)
                return false;
            return old(self, pid, e);
        });


        removeHfLogs = Ref.auto(GameManager.logAction).replace(function(hf: Hf, self: GameManager, str: String): Void {
            /* No logging, remove them. */
        });

        removeMapIcons = Ref.auto(GameMode.registerMapEvent).replace(function(hf: Hf, self: GameMode, eid: Int, misc: Dynamic): Void {
            /* We don't register map icons, it is pointless since we don't display the map.
             * Plus it makes the player respawn take more and more time as it fills with death
             * events (not noticable at the beginning but it is after a few thousand deaths). */
        });

        logCristal = Ref.auto(SpecialManager.executeExtend).before(function(hf: Hf, self: SpecialManager, fl_perfect: Bool): Void {
            log.logCristal(currentLevelId, randomfestTimeToString());
        });

        logSpecialItem = Ref.auto(GameMode.pickUpSpecial).before(function(hf: Hf, self: GameMode, id: Int): Void {
            /* Don't log cristal letters. */
            if (id == 0)
                return;

            var object = null;
            for (i in 0...hf.Data.SPECIAL_ITEM_FAMILIES.length) {
                var specFamily = hf.Data.SPECIAL_ITEM_FAMILIES[i];
                for (j in 0...specFamily.length) {
                    if (specFamily[j].id == id) {
                        object = specFamily[j];
                        break;
                    }
                }
                if (object != null)
                    break;
            }

            var name = object.name;
            if (name != "" && name != null)
                log.logSpecialItem(currentLevelId, name);
        });

        /* Needed for the various push darkness to be effective. Changing the value in the GameParams would be wrong as it is needed only for this option... */
        // TODO: Should be in SkyUI?
        forceAbilityToSetDarknessFromBegin = Ref.auto(GameMode.initGame).before(function(hf: Hf, self: GameMode): Void {
            hf.Data.MIN_DARKNESS_LEVEL = 0;
        });

        replacePauseWithPlayerLog = Ref.auto(GameMode.onPause).after(function(hf: Hf, self: GameMode): Void {
            /* Remove sprites originally added by the game that we don't want. */
            self.pauseMC.removeMovieClip();

            /* Add our own sprites. */
            SkyUI.PushDarknessAndLightSources(self, 75);
            log.drawPlayerLog(hf, self);
            makePlayerNamesInvisible(self.getPlayerList());
        });

        replaceMapWithRandomfestLog = Ref.auto(GameMode.onMap).replace(function(hf: Hf, self: GameMode): Void {
            /* The original part of the method that we keep. */
            self.fl_pause = true;
            self.lock();
            self.world.lock();
            if (!self.fl_mute)
                self.setMusicVolume(0.5);

            /* Add our own sprites. */
            SkyUI.PushDarknessAndLightSources(self, 75);
            log.drawRandomfestLog(hf, self);
            makePlayerNamesInvisible(self.getPlayerList());
        });

        removeCustomLogOnResume = Ref.auto(GameMode.onUnpause).after(function(hf: Hf, self: GameMode): Void {
            /* Remove our sprites and restore what we changed. */
            SkyUI.PopDarknessAndLightSources(self);
            log.eraseLog();
            makePlayerNamesVisible(self.getPlayerList());
        });

        // TODO: The following from the AS mod is not possible anymore (overriding basic nodes in merlin).
        //       Some changes in this area will be needed for better scripts anyway, so come back to this when
        //       betterscript is in the pipe.

        // /* Hack: Otherwise the e_portal event is consumed and nothing happens at the moment (because the dimensions are not activated).
        //  * Then the player activates them but the event is not there anymore and the door/vortex cannot be taken anymore.
        //  * In some places this could be a softlock (for example inside of dim16). */
        // EL.require("scripts.BetterScript").registerTest("t_pos enter", function(node) {
        //  for (var i = 0; i < node.children.length; ++i) {
        //      if (node.children[i].name == "e_portal")
        //          return this_mod.dimensions_activated;
        //  }
        //  return true;
        // });
    }
}