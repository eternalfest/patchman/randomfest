package randomfest;

import etwin.flash.MovieClip;
import etwin.Obfu;
import hf.mode.GameMode;
import hf.entity.Player;
import hf.Hf;
import patchman.DebugConsole;
import sky_ui.SkyUI;

private typedef LevelId = {
    did: Int,
    lid: Int
}

private enum LogEvent {
    LogEventLevelSummary(level: LevelId, nbDeath: Int, time: String);
    LogEventCristal(level: LevelId, time: String);
    LogEventControlGain(level: LevelId, nb: Int);
    LogEventControlUse(level: LevelId, time: String);
    LogSpecialItem(level: LevelId, objectName: String);
}

private enum CurrentLog {
    NoLog;
    PlayerLog;
    RandomfestLog;
}

class RandomfestLog {
    private var entries: Array<LogEvent>;
    private var currentLog: CurrentLog = NoLog;

    private static inline var VERTICAL_ARROW_SPRITE_OFFSET: Int = 30;
    private static inline var VERTICAL_POST_TOP_ARROW_OFFSET: Int = VERTICAL_ARROW_SPRITE_OFFSET;

    private static inline var NB_PLAYERS_DISPLAYED_IN_LOG: Int = 10;
    private static inline var VERTICAL_SPACE_BETWEEN_PLAYER_ENTRIES: Int = 40;
    private static var CRISTAL_LETTERS: Array<String> = [Obfu.raw("c"), Obfu.raw("r"), Obfu.raw("i"), Obfu.raw("s"), Obfu.raw("t"), Obfu.raw("a"), Obfu.raw("l")];

    private static inline var NB_MAX_LEVEL_STATS_DISPLAYED_IN_LOG: Int = 22;
    private static inline var VERTICAL_SPACE_BETWEEN_LOG_ENTRIES: Int = 20;

    private var logSprites: Array<MovieClip>;
    private var lastDisplayedEntryIndex: Int;

    public function logLevelSummary(currentLevelId: LevelId, nbDeathCurrentLevel: Int, time: String): Void {
        entries.push(LogEventLevelSummary(currentLevelId, nbDeathCurrentLevel, time));
    }

    public function logCristal(currentLevelId: LevelId, time: String): Void {
        entries.push(LogEventCristal(currentLevelId, time));
    }

    public function logControlGain(currentLevelId: LevelId) {
        /* We check the last 2 entries of the log instead of just the last if there is a cristal or something between the previous control gain and this one. */
        for (i in Math.floor(Math.max(0, entries.length - 2))...entries.length) {
            switch (entries[i]) {
                case LogEventControlGain(level, nb): ++nb; // TODO: Check if properly updated.
                default:
            }
        }
        entries.push(LogEventControlGain(currentLevelId, 1));
    }

    public function logControlUse(currentLevelId: LevelId, time: String): Void {
        entries.push(LogEventControlUse(currentLevelId, time));
    }

    public function logSpecialItem(currentLevelId: LevelId, objectName: String): Void {
        entries.push(LogSpecialItem(currentLevelId, objectName));
    }

    private static function levelIdToString(level: LevelId): String {
        if (level.did == 0)
            return Std.string(level.lid);
        else
            return Std.string(level.did) + "-" + Std.string(level.lid);
    }

    public static function logEventToString(event: LogEvent): String {
        switch (event) {
            case LogEventLevelSummary(level, nbDeath, time):
                return "Niveau " + levelIdToString(level) + " ~ Vies perdues : " + nbDeath + " , Temps à la fin : " + time;
            case LogEventCristal(level, time):
                return "Cristal au niveau " + levelIdToString(level) + ".";
            case LogEventControlGain(level, nb):
                var multiplicity_text: String = nb > 1 ? " (x" + nb + ")" : "";
                return "Gain d'une reprise de contrôle au niveau " + levelIdToString(level) + multiplicity_text + ".";
            case LogEventControlUse(level, time):
                return "Reprise de contrôle au niveau " + levelIdToString(level) + " à " + time;
            case LogSpecialItem(level, objectName):
                return "Objet '" + objectName + "' au niveau " + levelIdToString(level) + ".";
        }
    }

    // TODO: Drawing scrollable logs could probably be in SkyUI.
    // TODO: Move player log in its own file?

    public static function CreateArrow(hf: Hf, game: GameMode, down: Bool, x: Int, y: Int, scale: Float): MovieClip {
        var arrow_sprite: Dynamic = game.depthMan.attach("hammer_fx_pointer", hf.Data.DP_TOP);
        arrow_sprite._xscale = scale;
        arrow_sprite._yscale = scale;
        arrow_sprite._x = x;
        arrow_sprite._y = y;
        if (down)
            arrow_sprite._rotation = 180;
        arrow_sprite.gotoAndPlay("loop");
        return arrow_sprite;
    }

    private function CreatePlayerStatus(hf: Hf, game: GameMode, x: Int, y: Int, player: Player): Void {
        logSprites.push(SkyUI.CreateNormalTextSprite(hf, game, player.name, x, y, 120));

        var cristal: String = "";
        for (i in 0...player.extendList.length) {
            if (player.extendList[i])
                cristal += CRISTAL_LETTERS[i];
            else
                cristal += "  ";
        }

        logSprites.push(SkyUI.CreateNormalTextSprite(hf, game, cristal, x + 200, y, 120));
        logSprites.push(SkyUI.CreateNormalTextSprite(hf, game, Std.string(player.score), x + 270, y, 120));
    }

    private function drawPlayerLogInternal(hf: Hf, game: GameMode): Void {
        var players: Array<Player> = game.getPlayerList();

        var beginIndex: Int = lastDisplayedEntryIndex - Std.int(Math.min(players.length, NB_PLAYERS_DISPLAYED_IN_LOG));

        for (i in beginIndex...lastDisplayedEntryIndex)
            CreatePlayerStatus(hf, game, 25, VERTICAL_SPACE_BETWEEN_PLAYER_ENTRIES * (i - beginIndex) + VERTICAL_POST_TOP_ARROW_OFFSET, players[i]);

        if (CanScrollPlayerLogUp())
            logSprites.push(CreateArrow(hf, game, false, 190, 2, 80));
        if (CanScrollPlayerLogDown(players))
            logSprites.push(CreateArrow(hf, game, true, 190, VERTICAL_SPACE_BETWEEN_PLAYER_ENTRIES * NB_PLAYERS_DISPLAYED_IN_LOG + VERTICAL_POST_TOP_ARROW_OFFSET + 20, 80));
        logSprites.push(SkyUI.CreateNormalTextSprite(hf, game, "Contrôles : R = Reprise de contrôle, E = Activation des dimensions", -10, VERTICAL_SPACE_BETWEEN_PLAYER_ENTRIES * NB_PLAYERS_DISPLAYED_IN_LOG + 2 * VERTICAL_ARROW_SPRITE_OFFSET - 5, 96));
    }

    private function CanScrollPlayerLogUp(): Bool {
        return lastDisplayedEntryIndex > NB_PLAYERS_DISPLAYED_IN_LOG;
    }

    private function CanScrollPlayerLogDown(players: Array<Player>): Bool {
        return lastDisplayedEntryIndex < players.length;
    }

    private function ScrollPlayerLog(hf: Hf, game: GameMode, scrollUp: Bool): Void {
        if (scrollUp) {
            if (!CanScrollPlayerLogUp())
                return;
            --lastDisplayedEntryIndex;
        }
        else {
            if (!CanScrollPlayerLogDown(game.getPlayerList()))
                return;
            ++lastDisplayedEntryIndex;
        }

        eraseLogSprites();
        drawPlayerLogInternal(hf, game);
    }

    private function drawRandomfestLogInternal(hf: Hf, game: GameMode): Void {
        var nbStatsDisplayed;
        var beginIndex;
        if (entries.length < NB_MAX_LEVEL_STATS_DISPLAYED_IN_LOG) {
            nbStatsDisplayed = entries.length;
            beginIndex = 0;
            lastDisplayedEntryIndex = entries.length;
        }
        else {
            nbStatsDisplayed = NB_MAX_LEVEL_STATS_DISPLAYED_IN_LOG;
            beginIndex = lastDisplayedEntryIndex - nbStatsDisplayed;
        }

        for(i in beginIndex...lastDisplayedEntryIndex){
            var logText = logEventToString(entries[i]);
            logSprites.push(SkyUI.CreateNormalTextSprite(hf, game, logText, 20, VERTICAL_SPACE_BETWEEN_LOG_ENTRIES * (i - beginIndex) + VERTICAL_POST_TOP_ARROW_OFFSET, 100));
        }

        if (CanScrollRandomfestLogUp())
            logSprites.push(CreateArrow(hf, game, false, 190, 2, 80));
        if (CanScrollRandomfestLogDown())
            logSprites.push(CreateArrow(hf, game, true, 190, VERTICAL_SPACE_BETWEEN_LOG_ENTRIES * nbStatsDisplayed + VERTICAL_POST_TOP_ARROW_OFFSET + 32, 80));
    }

    private function CanScrollRandomfestLogUp() {
        return lastDisplayedEntryIndex > NB_MAX_LEVEL_STATS_DISPLAYED_IN_LOG;
    }

    private function CanScrollRandomfestLogDown() {
        return lastDisplayedEntryIndex < entries.length;
    }

    private function ScrollRandomfestLog(hf: Hf, game: GameMode, scrollUp: Bool) {
        if (scrollUp) {
            if (!CanScrollRandomfestLogUp())
                return;
            lastDisplayedEntryIndex--;
        }
        else {
            if (!CanScrollRandomfestLogDown())
                return;
            lastDisplayedEntryIndex++;
        }

        eraseLogSprites();
        drawRandomfestLogInternal(hf, game);
    }

    public function ScrollLog(hf: Hf, game: GameMode, scrollUp: Bool): Void {
        switch (currentLog) {
            case PlayerLog: ScrollPlayerLog(hf, game, scrollUp);
            case RandomfestLog: ScrollRandomfestLog(hf, game, scrollUp);
            case NoLog: DebugConsole.error("Log scrolling called when no log is currently shown.");
        }
    }

    public function drawPlayerLog(hf: Hf, game: GameMode): Void {
        currentLog = PlayerLog;
        lastDisplayedEntryIndex = Std.int(Math.min(game.getPlayerList().length, NB_PLAYERS_DISPLAYED_IN_LOG));
        drawPlayerLogInternal(hf, game);
    }

    public function drawRandomfestLog(hf: Hf, game: GameMode): Void {
        currentLog = RandomfestLog;
        lastDisplayedEntryIndex = entries.length;
        drawRandomfestLogInternal(hf, game);
    }

    public function eraseLogSprites(): Void {
        for (logSprite in logSprites)
            logSprite.removeMovieClip();
        logSprites = [];
    }

    public function eraseLog(): Void {
        currentLog = NoLog;
        eraseLogSprites();
    }

    public function new() {
        entries = [];
        logSprites = [];
    }
}