package randomfest;

import etwin.Obfu;
import patchman.DebugConsole;
import patchman.module.Data;

@:build(patchman.Build.di())
class RandomfestConfig {
    public var nbInitialRemainingControl(default, null): Int;
    public var areDimensionsAlwaysActivated(default, null): Bool;
    public var nbDeathForControlGain(default, null): Int;
    public var nbAdditionalPlayers(default, null): Int;
    public var allowSpawningRandoms(default, null): Bool;

    public function new(nbInitialRemainingControl: Int, areDimensionsAlwaysActivated: Bool, nbDeathForControlGain: Int, nbAdditionalPlayers: Int, allowSpawningRandoms: Bool): Void {
        this.nbInitialRemainingControl = nbInitialRemainingControl;
        this.areDimensionsAlwaysActivated = areDimensionsAlwaysActivated;
        this.nbDeathForControlGain = nbDeathForControlGain;
        this.nbAdditionalPlayers = nbAdditionalPlayers;
        this.allowSpawningRandoms = allowSpawningRandoms;
    }

    public static var DEFAULT: RandomfestConfig = new RandomfestConfig(3, false, 2500, 8, false);

    private static var RANDOMFEST_PARAMS_CONFIG(default, never): String = Obfu.raw("Randomfest");

    private static var NB_INITIAL_REMAINING_CONTROL(default, never): String = Obfu.raw("nb_initial_remaining_control");
    private static var ARE_DIMENSIONS_ALWAYS_ACTIVATED(default, never): String = Obfu.raw("are_dimensions_always_activated");
    private static var NB_DEATH_FOR_CONTROL_GAIN(default, never): String = Obfu.raw("nb_death_for_control_gain");
    private static var NB_ADDITIONAL_PLAYERS(default, never): String = Obfu.raw("nb_additional_players");
    private static var ALLOW_SPAWNING_RANDOM_PLAYER_OUTSIDE_OPTION(default, never): String = Obfu.raw("allow_spawning_random_player_outside_option");

    @:diFactory
    public static function fromHml(dataMod: Data): RandomfestConfig {
        if (!dataMod.has(RANDOMFEST_PARAMS_CONFIG)) {
            DebugConsole.warn("Randomfest.json is missing from the data folder, using default values.");
            return RandomfestConfig.DEFAULT;
        }

        var data: Dynamic = dataMod.get(RANDOMFEST_PARAMS_CONFIG);

        var nbControls: Null<Int> = Reflect.field(data, NB_INITIAL_REMAINING_CONTROL);
        if (nbControls == null) {
            DebugConsole.error("'nb_initial_remaining_control' value is missing from Randomfest.json.");
            return RandomfestConfig.DEFAULT;
        }

        var dimensionsActive: Null<Bool> = Reflect.field(data, ARE_DIMENSIONS_ALWAYS_ACTIVATED);
        if (dimensionsActive == null) {
            DebugConsole.warn("'are_dimensions_always_activated' value is missing from Randomfest.json, using default value (false) for it.");
            dimensionsActive = false;
        }

        var nbDeaths: Null<Int> = Reflect.field(data, NB_DEATH_FOR_CONTROL_GAIN);
        if (nbDeaths == null) {
            DebugConsole.error("'nb_death_for_control_gain' value is missing from Randomfest.json.");
            return RandomfestConfig.DEFAULT;
        }

        var nbPlayers: Null<Int> = Reflect.field(data, NB_ADDITIONAL_PLAYERS);
        if (nbPlayers == null) {
            DebugConsole.error("'nb_additional_players' value is missing from Randomfest.json.");
            return RandomfestConfig.DEFAULT;
        }

        var allowSpawningRandoms: Null<Bool> = Reflect.field(data, ALLOW_SPAWNING_RANDOM_PLAYER_OUTSIDE_OPTION);
        if (allowSpawningRandoms == null)
            allowSpawningRandoms = false;

        return new RandomfestConfig(nbControls, dimensionsActive, nbDeaths, nbPlayers, allowSpawningRandoms);
    }
}
