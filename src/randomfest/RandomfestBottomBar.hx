package randomfest;

import etwin.flash.MovieClip;
import bottom_bar.BottomBarInterface;
import bottom_bar.modules.BottomBarModule;
import randomfest.Randomfest;
import randomfest.RandomfestLog;
import sky_ui.SkyUI;

class RandomfestBottomBar extends BottomBarModule {
    private var randomfest: Randomfest;

    public function new(randomfest: Randomfest) {
        this.randomfest = randomfest;
    }

    public override function init(bottomBar: BottomBarInterface): Void {
        /* Remove the possibility of a '+' sprite. */
        bottomBar.more = null;

        var hf = bottomBar.game.root;

        /* Normal base interface. */
        bottomBar.mc = (cast bottomBar.game.depthMan.attach("hammer_interf_game", hf.Data.DP_TOP));
        bottomBar.mc._x = -bottomBar.game.xOffset;
        bottomBar.mc._y = cast hf.Data.DOC_HEIGHT;
        bottomBar.mc.gotoAndStop("3");
        bottomBar.mc.cacheAsBitmap = true;

        /* Custom stuff. */
        var totalDeathIcon: MovieClip = bottomBar.game.depthMan.attach("hammer_interf_mapIcon", hf.Data.DP_TOP);
        totalDeathIcon._x = 0;
        totalDeathIcon._y = 510;
        totalDeathIcon._xscale = 200;
        totalDeathIcon._yscale = 200;
        totalDeathIcon.gotoAndStop("" + hf.Data.EVENT_DEATH);
        (cast bottomBar).totalDeathIcon = totalDeathIcon;
        (cast bottomBar).nbDeathTotal = SkyUI.CreateNormalTextSprite(hf, bottomBar.game, "0", 10, 499, 115);

        var letterC: MovieClip = bottomBar.game.depthMan.attach('hammer_item_special', hf.Data.DP_TOP);
        letterC.gotoAndStop("1");
        letterC._x = 80;
        letterC._y = 518;
        letterC._xscale = 70;
        letterC._yscale = 70;
        (cast bottomBar).letterC = letterC;
        (cast bottomBar).nbRemainingControl = SkyUI.CreateNormalTextSprite(hf, bottomBar.game, "0", 90, 499, 115);

        var nextLevelArrow: MovieClip = RandomfestLog.CreateArrow(hf, bottomBar.game, true, 130, 516, 50);
        nextLevelArrow.gotoAndStop('loop');
        nextLevelArrow._xscale = 30;
        (cast bottomBar).nextLevelArrow = nextLevelArrow;
        (cast bottomBar).level = SkyUI.CreateNormalTextSprite(hf, bottomBar.game, "0", 135, 499, 115);

        var levelDeathIcon: MovieClip = bottomBar.game.depthMan.attach("hammer_interf_mapIcon", hf.Data.DP_TOP);
        levelDeathIcon._x = 175;
        levelDeathIcon._y = 510;
        levelDeathIcon._xscale = 200;
        levelDeathIcon._yscale = 200;
        levelDeathIcon.gotoAndStop("" + hf.Data.EVENT_DEATH);
        (cast bottomBar).levelDeathIcon = levelDeathIcon;
        (cast bottomBar).nbDeathLevel = SkyUI.CreateNormalTextSprite(hf, bottomBar.game, "0", 185, 499, 115);

        (cast bottomBar).currentTime = SkyUI.CreateNormalTextSprite(hf, bottomBar.game, "", 395, 499, 115, null, TextAlignmentRight);
    }

    public override function setLevel(pId: Int, bottomBar: BottomBarInterface): Bool {
        (cast bottomBar).level.field.text = Std.string(pId);
        return false;
    }

    public override function update(bottomBar: BottomBarInterface): Bool {
        (cast bottomBar).nbDeathTotal.field.text = Std.string(randomfest.getNbDeathTotal());
        (cast bottomBar).nbRemainingControl.field.text = Std.string(randomfest.getNbRemainingControl());
        (cast bottomBar).nbDeathLevel.field.text = Std.string(randomfest.getNbDeathCurrentLevel());
        (cast bottomBar).currentTime.field.text = randomfest.randomfestTimeToString();
        return false;
    }

}