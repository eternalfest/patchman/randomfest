# 3.0.0 (2023-11-05)

- **[Breaking change]** Update to `color_matrix@0.11.0`.
- **[Change]** Update to `bottom_bar@0.2.2`.
- **[Change]** Set `preferUnplugged` to `true`.
- **[Internal]** Update to Yarn 4.

# 2.0.0 (2021-04-22)

- **[Breaking change]** Update to `patchman@0.10.4`.
- **[Change]** Use `bottom_bar@0.2.0` package.
- **[Internal]** Update to Yarn 2.

# 1.4.1 (2021-03-17)

- **[Fix]** Remove local debug code before publishing

# 1.4.0 (2021-03-17)

- **[Fix]** Upgrade bottom bar dependency.
- **[Feature]** Properly handle dynamically changing the number of players
- **[Fix]** Make behavior coherent with message (on activated dimensions config)
- **[Feature]** Configuration of number of players and lives needed for control
- **[Feature]** Handle having both random and non random players
- **[Feature]** Allow spawning random players outside of the option

# 1.3.0 (2021-02-20)

- **[Fix]** Use .name instead of ._name for the player names. Needed for compatibility with name display feature in shadow clones 0.4.0.

# 1.2.0 (2021-01-19)

- **[Feature]** Update eternal names.
- **[Feature]** Add new player names.
- **[Feature]** Increase probability of having non-eternal names.

# 1.1.0 (2020-12-14)

- **[Feature]** Add function to query whether the option is enabled.

# 1.0.0 (2020-12-14)

- **[Feature]** First release.
